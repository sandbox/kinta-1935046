<?php
/**
 * @file og_extra_rules.rules.inc
 * TODO: Enter file description here.
*/
/**
 * Implements hook_rules_action_info().
 */
function og_extra_rules_rules_action_info() {
  return array(
//VALID
    'og_get_node' => array(
      'label' => t('[Valid]Get related node from membership entity'),
      'group' => t('Organic groups'),
      'parameter' => array(
        'value' => array(
          'type' => 'og_membership',
          'label' => t('OG membership'),
          'description' => t('The group membership entity.'),
        ),
      ),
      'provides' => array(
        'related_content' => array('type' => 'list<node>', 'label' => t('the related group bundle')),
      ),
      'base' => 'og_extra_rules_rules_get_node',
      'access callback' => 'og_extra_rules_rules_integration_access',
    ), 
//VALID
    'og_get_og_members_by_group'=> array(
      'label'=>t('[Valid]Get group members from group node'),
      'group'=>t('Organic groups'),
      'parameter'=>array(
        'group'=> array(
          'type'=>'node',
          'label'=> t('Group node'),
          'description'=>t('The group node'),
        ),
      ),
      'provides'=>array(
        'group_members' => array('type' => 'list<user>', 'label' => t('List of group members')),
      ),
      'base'=> 'og_extra_rules_get_members_by_group',
      'access callback'=>'og_extra_rules_rules_integration_access',     
    ), 
//TODO
    'og_get_og_members' => array(
      'label' => t('[NotTested]Get all group members from group membership'),
      'group' => t('Organic groups'),
      'parameter' => array(
        'group_content' => array(
          'type' => 'entity',
          'label' => t('OG Membership'),
          'description' => t('The group membership entity.'),
        ),
        // @todo: Add option to filter by member-state?
      ),
      'provides' => array(
        'group_members' => array('type' => 'list<user>', 'label' => t('List of group members')),
      ),
      'base' => 'og_extra_rules_get_og_members',
      'access callback' => 'og_extra_rules_rules_integration_access',
    ),
// TODO
    'og_get_gid' => array(
      'label' => t('[NotTested]Get group id from membership entity'),
      'group' => t('Organic groups'),
      'parameter' => array(
        'group_content' => array(
          'type' => 'og_membership',
          'label' => t('OG membership'),
          'description' => t('The group membership entity.'),
        ),
      ),
      'provides' => array(
        'group_id' => array('type' => 'list<integer>', 'label' => t('ids of the group')),
      ),
      'base' => 'og_extra_rules_rules_get_gid',
      'access callback' => 'og_extra_rules_rules_integration_access',
    ),

  );
}

/**
 * Action: Get related nodes from a group content.
 */
function og_extra_rules_rules_get_node($value){
   $membership=$value;
   $r_content=array();
   $content = entity_load_single($membership->group_type, $membership->gid);
   // Get the group related node the group content belongs to
   array_push($r_content,$content);
   return array( "related_content" => $r_content );
}

/*
 * Action: get members from a group
 */
function og_extra_rules_get_members_by_group($group_node){
  $gid=$group_node->nid;
  $members = array();
    // Get the group members the group content belongs to.
    $current_members = db_select('og_membership', 'om')
      ->fields('om', array('etid'))
      ->condition('om.gid', $gid) //$og_membership->gid)
      ->condition('om.group_type', 'node') //$og_membership->group_type)
      ->condition('om.entity_type', 'user')
      ->execute()
      ->fetchCol();

  $members = array_merge($members, $current_members);
  // Remove duplicate items.
  $members = array_keys(array_flip($members));
  return array('group_members' => $members);
}



/**
 * Action: Get group ids from a group membership
 * TODO NOT TESTED
 */
function og_extra_rules_rules_get_gid($membership){
    if (!isset($membership->og_membership)) {
      return;
    }
    print "<pre>";
    $gids=array();
    foreach ($membership->og_membership->value() as $og_membership) {
      // Get the group ids from the group membership
      $gid=$og_membership->gid;
      array_push($gids,$gid);
//      db_select('node','n')
    };
    return array( "group_id" => $gids );
}



/**
 * Action: Get group members from a group membership.
 * TODO NOT TESTED
 */
function og_extra_rules_get_og_members($membership) {
  if (!isset($membership->og_membership)) {
    // Not a group content.
    return;
  }

  $members = array();
  foreach ($membership->og_membership->value() as $og_membership) {
    // Get the group members the group membership belongs to.
    $current_members = db_select('og_membership', 'om')
      ->fields('om', array('etid'))
      ->condition('om.gid', $og_membership->gid)
      ->condition('om.group_type', $og_membership->group_type)
      ->condition('om.entity_type', 'user')
      ->execute()
      ->fetchCol();

    $members = array_merge($members, $current_members);
  }
  // Remove duplicate items.
  $members = array_keys(array_flip($members));
  return array('group_members' => $members);
}

/*
 * Action: Get related nodes from a group content.
 * TODO NOT DEFINED ACTION
 */
function og_extra_rules_rules_get_node_from_audience($value){
$membership=$value;
dpm("this will be membership?");
dpm($membership);
dpm('not a group?');

    if (!isset($membership->og_membership)) {
dpm('not a group');
      return;
    }
dpm('group ms');
    $r_content=array();
    
    foreach ($membership->og_membership->value() as $og_membership) {
      $content = entity_load_single($og_membership->group_type, $og_membership->gid);
      // Get the group related node the group content belongs to
      array_push($r_content,$content);
    };
    return array( "related_content" => $r_content );
}



/**
 * OG Rules integration access callback.
 */
function og_extra_rules_rules_integration_access($type, $name) {
  // Grant everyone access to conditions.
  return $type == 'condition' || user_access('administer group');
}
