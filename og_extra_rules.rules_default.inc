<?php
/**
 * @file
 * og_extra_rules.rules_defaults.inc
 */

/**
 * Implementation of hook_default_rules_configuration().
 */
function og_extra_rules_default_rules_configuration() {
  $items = array();
  $items['subgroup_event_propagation_example'] = entity_import('rules_config', '{ "subgroup_event_propagation_example" : {
    "LABEL" : "Subgroup event propagation example",
    "PLUGIN" : "reaction rule",
    "TAGS" : [ "subgroup content" ],
    "REQUIRES" : [ "rules", "og", "og_extra_rules", "php" ],
    "ON" : [ "node_view" ],
    "IF" : [
      { "node_is_of_type" : {
          "node" : [ "node" ],
          "type" : { "value" : { "subgroup_content" : "subgroup_content" } }
        }
      },
      { "og_entity_is_group_content" : { "entity" : [ "node" ] } }
    ],
    "DO" : [
      { "og_get_members" : {
          "USING" : { "group_content" : [ "node" ] },
          "PROVIDE" : { "group_members" : { "subgroup_group_members" : "List of subgroup members" } }
        }
      },
      { "LOOP" : {
          "USING" : { "list" : [ "node:og-membership" ] },
          "ITEM" : { "list_subgroups_item" : subgroups list item" },
          "DO" : [
            { "og_get_og_members" : {
                "USING" : { "group_content" : [ "list-subgroups-item:group" ] },
                "PROVIDE" : { "group_members" : { "groups_group_members" : "List of related groups group members" } }
              }
            },
            { "drupal_message" : { "message" : "\u003C?php\r\nprint \u0022Subgroup Content:\u0022.$node-\u003Etitle;\r\nprint \u0022\u003Cbr\/\u003E\u0026nbsp;\u0026nbsp;\u0026nbsp;\u0026nbsp;\\_ child of the subgroup : \u0022.node_load($list_subgroups_item-\u003Egid)-\u003Etitle.\u0022 ( \u0022.$list_subgroups_item-\u003Egid.\u0022 )\u0022;\r\n?\u003E" } },
            { "og_get_node" : {
                "USING" : { "group_content" : [ "list-subgroups-item:group" ] },
                "PROVIDE" : { "related_content" : { "related_content" : "the related group bundle" } }
              }
            },
            { "LOOP" : {
                "USING" : { "list" : [ "related-content" ] },
                "ITEM" : { "list_groups_item" : "groups list item" },
                "DO" : [
                  { "drupal_message" : { "message" : "\u003C?php\r\nprint \u0022\u003Cbr\/\u003E\u0026nbsp;\u0026nbsp;\u0026nbsp;\u0026nbsp;\u0026nbsp;\u0026nbsp;\u0026nbsp;\u0026nbsp;\\_ child of the group [list_groups_item:title] ([list_groups_item:nid])\u0022;\r\n?\u003E" } }
                ]
              }
            },
            { "drupal_message" : { "message" : "All groups users list:" } },
            { "LOOP" : {
                "USING" : { "list" : [ "groups-group-members" ] },
                "ITEM" : { "subgroup_user_item" : "subgroup user" },
                "DO" : [
                  { "drupal_message" : { "message" : "\u0026nbsp;\u0026nbsp;\u0026nbsp;\u0026nbsp;[subgroup-user-item:name]\r\n" } }
                ]
              }
            }
          ]
        }
      },
      { "drupal_message" : { "message" : "Subgroup users list:" } },
      { "LOOP" : {
          "USING" : { "list" : [ "subgroup-group-members" ] },
          "ITEM" : { "subgroup_user_item" : "user from users list in a subgroup" },
          "DO" : [
            { "drupal_message" : { "message" : "\u0026nbsp;\u0026nbsp;\u0026nbsp;\u0026nbsp;[subgroup-user-item:name]" } }
          ]
        }
      }
    ]
  }
}');
  return $items;
}


